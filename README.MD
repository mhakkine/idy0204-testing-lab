# Requirements
npm, npx, Chrome or Chromium.

On windows systems you should already have npx if npm is installed.
For unix systems please try `sudo npm i -g npx`

# How to run these tests

1) Download cypress from: https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements


2) Install the required node packages by `npm i`


3) Run cypress by `npx cypress open`


4) Select the spec file `BetriumTests.js` in the test runner and press run


5) If needed then allow cypress through the firewall