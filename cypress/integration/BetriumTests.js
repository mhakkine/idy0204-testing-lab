context('Betrium Tests', () => {
  let userName = "quality100@gmail.com";
  let failUserName = "qualityFail@gmail.com";
  let password = "Qwe";

  beforeEach(() => {
    cy.visit('https://betrium.ga')
  })

  it('Given I am logged out user and I enter valid credentials then I will be logged in', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	cy.wait(500);
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	expect(cy.get('.name.dropdown-handler').should('be.visible'));
  })
  
  it('Given I am logged out user and I enter invalid credentials then an error message will be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(failUserName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	cy.wait(1000);
	//End login

	expect(cy.get('form#signin').should('be.visible'))
  })
  
  it('Given I am logged out user and I would like to register with existing credentials, then an error message should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	cy.get('.signup').first().click()
	cy.get('#signup > input.email').type(userName);
	cy.get('#signup > input.fullname').type('Tester');
	cy.get('#signup > input.password').type(password);
	cy.get('#signup > input.password2').type(password);
	cy.get('#signup > input.country').type('Transylvania');
	cy.get('#signup > input.nickname').type('Nick22');
	cy.get('#signup > .send').first().click()
	cy.wait(1000);

	expect(cy.get('form#signup').should('be.visible'));
  })
  
  it('Given I am logged out user and I would like to register with non-existing credentials, then an success message should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	cy.get('.signup').first().click();;
	let randomId = Math.ceil(Math.random() * 1000000000000);
	cy.get('#signup > input.email').type('Quality@' + randomId + 'gmail.com');
	cy.get('#signup > input.fullname').type('Tester ' + randomId );
	cy.get('#signup > input.password').type(password);
	cy.get('#signup > input.password2').type(password);
	cy.get('#signup > input.country').type('Transylvania');
	cy.get('#signup > input.nickname').type('Nick' + randomId);
	cy.get('#signup > .send').first().click();
	cy.wait(1000);
	expect(cy.get('#overlay > #success').should('be.visible'))
  })
  
  it('Given I am logged in/logged out user and observe the footer, then I can see license information', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	expect(cy.get('.disclaimers').should('be.visible'));
	expect(cy.get('.disclaimers').contains('The presented solution is an intellectual property of BETRIUM HOLDING LTD. registered under Hong Kong jurisdiction. The solution is not an actual gambling platform and can not be taken as a real online gambling or sports betting website. The presented solution is not currently covered by any online gambling license. The solution is available for demonstrating and testing purposes only.'));
  })
  
  it('Given I am logged in and I observe the header then I can see my user name and balance', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  })

  it('Given I am logged in/logged out user and I search for an event with valid string and press submit, then events are shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	cy.get('.wrap_right > .search > input').type('a');
	cy.wait(200);
	cy.get('.wrap_right > .search > input').first().focus()
	expect(cy.get('.wrap_right > .search > .items').should('be.visible'));
  })
  
  
  it('Given I am logged in/logged out user and I search for an event with invalid string and press submit, then events are not shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	cy.get('.wrap_right > .search > input').type('$@');
	cy.wait(200);
	cy.get('.wrap_right > .search > input').first().focus();
	expect(cy.get('.wrap_right > .search > .items').should('not.be.visible'));
  })

  it('Given I am logged in user and I select my bet history, then I can see my bet history', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click()
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.name.dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.profile > div.dropdown.show > a:nth-child(1)').first().click();
	expect(cy.get('body > div.page.page_account > div.account > div.history > div').should('be.visible'));
  })
 
  it('Given I am logged in user and I select my account, then I can see my account section', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}))
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.name.dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.profile > div.dropdown.show > a:nth-child(2)').first().click();
	expect(cy.get('body > div.page.page_account > form').should('be.visible'));
	expect(cy.get('body > div.page.page_account > form > input.email').value = "userName");
  })
  
  it('Given I am logged in user and I want to deposit with bitcoin and I select deposit, then I should be able to see deposit address', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.balance > .dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.balance > div.dropdown.show > a:nth-child(1)').first().click();
	expect(cy.get('body > div.page.page_account > div.bank > div.deposit > div.tabs > div.tab.btc.show > div.about > p:nth-child(1) > span').should('be.visible'));
	expect(cy.get('body > div.page.page_account > div.bank > div.deposit > div.tabs > div.currency > div.select').contains('BTC'));
  })
  
  
  it('Given I am logged in user and I want to deposit with ethereum and I select deposit, then I should be able to see deposit address', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}))
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'))
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'))
  	cy.get('.balance > .dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.balance > div.dropdown.show > a:nth-child(1)').first().click();
	expect(cy.get('body > div.page.page_account > div.bank > div.deposit > div.tabs > div.currency > div:nth-child(2)').first().click());
	expect(cy.get('body > div.page.page_account > div.bank > div.deposit > div.tabs > div.tab.eth.show > div.about > p:nth-child(1) > span').should('be.visible'));
	expect(cy.get('body > div.page.page_account > div.bank > div.deposit > div.tabs > div.currency > div.select').contains('ETH'));
  })
  
  it('Given I am logged in user and I want to withdraw and I select withdraw, then I should be able to see the forms for withdrawal', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.balance > .dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.balance > div.dropdown.show > a:nth-child(2)').first().click();
	expect(cy.get('body > div.page.page_account > div.bank > div.withdraw').should('be.visible'));
  })

  it('Given I am logged in user and I want to see my payment history and I select the section from account page, then I am able to see my transaction history', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.balance > .dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.balance > div.dropdown.show > a:nth-child(3)').first().click();
	cy.get('body > div.page.page_account > div.account_menu > a:nth-child(5)').first().click();
	expect(cy.get('body > div.page.page_account > div.list').should('be.visible'));
	expect(cy.get('body > div.page.page_account > div.custom').should('be.visible'));
  })
  
  
  it('Given I am logged in user and I want to add my own custom event and I navigate to Create Custom Event, then the form for creating custom events will be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	//End login

	expect(cy.get('.name.dropdown-handler').should('be.visible'));
	expect(cy.get('.balance>div.dropdown-handler').should('be.visible'));
  	cy.get('.balance > .dropdown-handler').first().click();
	cy.get('#menu > div.wrap_right > div.balance > div.dropdown.show > a:nth-child(3)').first().click();
	cy.get('body > div.page.page_account > div.account_menu > a:nth-child(5)').first().click();
	expect(cy.get('body > div.page.page_account > div.list').should('be.visible'));
	expect(cy.get('body > div.page.page_account > div.custom').should('be.visible'));
  })
  
  it('Given I am logged out user and I try to place a bet then "unauthorized" message should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	cy.get('#page > div.wrap_right_big.tournaments > div:nth-child(6) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > span:nth-child(1)').first().click();
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(5) > button').first().click();
	
	var alerted = false;
    cy.on('window:alert', msg => alerted = msg);
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.total.show > div.body > div:nth-child(2) > div').first().click();
	cy.wait(1000).then(() => expect(alerted).to.equal("Not authorized!"));
  })
  
  it('Given I am logged in user and I place a bet with funds that I have then the success message should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	cy.wait(1000);
	//End login
	
	cy.get('#page > div.wrap_right_big.tournaments > div:nth-child(8) > div:nth-child(1) > div:nth-child(2) > div:nth-child(7)').first().click();
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(4) > input').type(0.01, {force:true});
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(5) > button').first().click();
	
	var alerted = false;
    cy.on('window:alert', msg => alerted = msg);
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.total.show > div.body > div:nth-child(2) > div').first().click();
	//cy.get('#success').should('be.visible');
  })
  
  it('Given I am logged in user and I place a bet with more funds that I have then an error message should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	cy.wait(1000);
	//End login
	
	cy.get('#page > div.wrap_right_big.tournaments > div:nth-child(8) > div:nth-child(1) > div:nth-child(2) > div:nth-child(7)').first().click();
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(4) > input').type(5000, {force:true});
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(5) > button').first().click();
	
	var alerted = false;
    cy.on('window:alert', msg => alerted = msg);
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.total.show > div.body > div:nth-child(2) > div').first().click();
	cy.wait(1000).then(() => expect(alerted).to.equal("No money on balance!"));
  })
	
  it('Given I am logged in user and I try to place a bet on a game that I have already bet on then an alert should be shown that I can not bet on the same match multiple times', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	cy.wait(1000)
	//End login
	
	cy.get('#page > div.wrap_right_big.tournaments > div:nth-child(6) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > span:nth-child(1)').first().click();
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(4) > input').type(0.01, {force:true});
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.betslip > div > div:nth-child(5) > button').first().click();
	
	var alerted = false;
    cy.on('window:alert', msg => alerted = msg);
	cy.get('#page > div.wrap_left.fixed > div.betslip_form > div.total.show > div.body > div:nth-child(2) > div').first().click();
	cy.wait(1000).then(() => expect(alerted).to.equal("Bet on this event already placed!"));
  })
  
  it('Given I am logged in user and I select on  the bell icon to see my notifications the notifications should be shown', () => {
	// Close the demo disclaimer modal
	cy.get('.show > div.close').first().click();
    expect(cy.get('#overlay').should(($div) => {$div == undefined}));
	
	//Start login
	cy.get('.signin').first().click();
	cy.get('#signin > input.email').type(userName);
	cy.get('#signin > input.password').type(password);
	cy.get('#signin > .send').first().click();
	cy.wait(1000);
	//End login
	
	cy.get('#menu > div.wrap_right > div.notifications > div.dropdown-handler').first().click();
	expect(cy.get('#menu > div.wrap_right > div.notifications > div.dropdown.show').should('be.visible'));
  })
})